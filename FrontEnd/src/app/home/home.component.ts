import { Component, OnInit, ViewChild } from '@angular/core';
import { Book } from '../shared/book.model';
import { ApiService } from '../shared/api.service';
import { DetailModalComponent } from './detail-modal/detail-modal.component';
import { Writer } from '../shared/writer.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})



export class HomeComponent implements OnInit {
  books: Book[] = [];
  witers: Writer[] = [];
  searchText: string;
  title: string;

  @ViewChild('detailModal') detailModal: DetailModalComponent;


  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getBooks().subscribe((data: Book[]) => {

      for (let i = 0; i < data.length; i++) {
        this.api.getBook(data[i].id).subscribe((info: Book) => {
          info.id = data[i].id;
          if (!info.img) {
           info.img = 'https://staging.rd.com/wp-content/uploads/2019/11/shutterstock_509582812-e1574100998595.jpg';
          }
        
          this.books.push(info);
          
          console.log(this.books);
        },
          (e: Error) => {
            console.log('err', e);
          });
      }
    },

   
      (er: Error) => {
        console.log('err', er);
      });
  }

  showDM(id: number): void {
    this.detailModal.initialize(id);
  }

}

// export class AppComponent{
//   play(){
//    console.log('play') 

//   }

//   pause(){
//   console.log('pause')


//   }

//   stop(){
//   console.log('stop')


//   }

// }
