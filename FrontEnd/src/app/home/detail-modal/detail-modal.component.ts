import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Publisher } from '../../shared/publisher.model';
import { ApiService } from '../../shared/api.service';
import { CartService } from '../../shared/cart.service';
import { Book } from 'src/app/shared/book.model';

@Component({
  selector: 'app-detail-modal',
  templateUrl: './detail-modal.component.html',
  styleUrls: ['./detail-modal.component.css']
})
export class DetailModalComponent implements OnInit {
  @ViewChild('detailModal') modal: ModalDirective;
  publisher = new Publisher();
  book = new Book();
 

  constructor(private api: ApiService, private cart: CartService) { }

  ngOnInit() {}

  initialize(id: number): void {
    //this.getPublisher(id);
    this.getBook(id);
    this.modal.show();
  }



  getBook(id: number) {
    this.api.getBook(id)
      .subscribe((data: Book) => {
        this.book = data;
      
      },
      (err: Error) => {
        console.log('err', err);

      });

    
  }

  //getStudio(id: number) {
    //this.api.getStudio(id)
     // .subscribe((data: any) => {
      //  this.studio = data.name;
     // },
      //  (err: Error) => {
       //   console.log('err', err);

      //  });
 // }

  getPublisher(id: number) {
    this.api.getPublisher(id)
      .subscribe((data: Publisher) => {
        this.publisher = data;
        this.publisher.id = id;
        if (!data.img) {
          this.publisher.img = 'https://i.ibb.co/0cBJC3N/3.jpg';
        }
  //      this.getStudio(this.publisher.studioId);
      },
        (err: Error) => {
          console.log('err', err);

        });
  }

  addToCart(book: Book) {
    this.cart.add(book);
    this.modal.hide();
  }

}
