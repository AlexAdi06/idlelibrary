import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, CheckboxRequiredValidator } from '@angular/forms';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  options = ['Book', 'Writer', 'Publisher'];
  selectedOption = 'Publisher';
  currentFormRef: any;
  addPublisherForm: FormGroup;//addAlbumForm
  addWriterForm: FormGroup;//addArtistForm
  addBookForm: FormGroup;//addSongForm
  success: boolean;

  constructor(public fb: FormBuilder, private api: ApiService) { }


  ngOnInit() {

    this.addPublisherForm = this.fb.group({
      //id: [null, Validators.required],
      name: [null, Validators.required],
      bookPublisher: [null,Validators.required],
      img: [null]
    });
    this.addWriterForm = this.fb.group({
     //id: [null, Validators.required],
      name: [null, Validators.required],
      nationality: [null, Validators.required],
      bookWriter: [null,Validators.required],
      writerCategory: [null, Validators.required]
    });
    this.addBookForm = this.fb.group({
      name: [null, Validators.required],
      popularity: [null, Validators.required],
      publicationYear: [null, Validators.required],
      writerId: [null, Validators.required],
      libraryId: [null, Validators.required],
      publisherId: [null, Validators.required],
      img: [null]
    });

    this.currentFormRef = this.addPublisherForm;

  }

  radioChange(event: any) {
    this.selectedOption = event.target.value;
    this.currentFormRef = this['add' + this.selectedOption + 'Form'];
    /*
      notatiile this.api si this[api] sunt echivalente

     folosind paratenze putem utiliza variabile

     this['add' + this.selectedOption + 'Form'] = this['addAlbumForm']  = this.addAlbumForm sau
                                                                        = this.addSongForm sau
                                                                        = this.addArtist
    */
  }

  add() {
    /*


    this.api['add' + this.selectedOption] = this.api['addAlbum'] = this.api.addAlbum sau
                                          = this.api['addSong] sau
                                          = this.api['addArtist]
    */
    this.api['add' + this.selectedOption](this.currentFormRef.value).subscribe(() => {

      this.currentFormRef.reset();
      this.success = true;
      setTimeout(() => {
        this.success = null;
      }, 3000);
    },
      (error: Error) => {
        console.log(error);
        this.currentFormRef.reset();
        this.success = false;
        setTimeout(() => {
          this.success = null;
        }, 3000);
      });

  }
}
