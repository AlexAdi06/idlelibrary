import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Writer } from '../../shared/writer.model';
import { ApiService } from '../../shared/api.service';


@Component({
  selector: 'app-edit-writer-modal',
  templateUrl: './edit-writer-modal.component.html',
  styleUrls: ['./edit-writer-modal.component.css']
})
export class EditWriterModalComponent {
  @ViewChild('editWriterModal') modal: ModalDirective;
  @Output() change: EventEmitter<string> = new EventEmitter<string>();
  editWriterForm: FormGroup;
  currentWriter = new Writer();

  constructor(public fb: FormBuilder, private api: ApiService) { }

  initialize(id: number): void {
    this.modal.show();
    this.api.getWriter(id)
      .subscribe((data: Writer) => {
        this.currentWriter = data;
        this.initializeFrom(this.currentWriter);
      },
        (error: Error) => {
          console.log('err', error);

        });
  }

  initializeFrom(currentWriter: Writer) {
    this.editWriterForm = this.fb.group({
      name: [currentWriter.name, Validators.required],
      nationality: [currentWriter.nationality, Validators.required],
    });
  }

  editWriter() {
    const editedWriter = new Writer({
      id: this.currentWriter.id,
      name: this.editWriterForm.value.name,
      nationality: this.editWriterForm.value.nationality,
    });

    this.api.editWriter(editedWriter)
      .subscribe(() => {
        this.change.emit('writer');
        this.modal.hide();
      },
        (error: Error) => {
          console.log('err', error);
        });
  }

}



