import { Component, EventEmitter, Output, ViewChild } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../shared/api.service';
import { Publisher } from '../../shared/publisher.model';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-edit-publisher-modal',
  templateUrl: './edit-publisher-modal.component.html',
  styleUrls: ['./edit-publisher-modal.component.css']
})
export class EditPublisherModalComponent {
  @ViewChild('editPublisherModal') modal: ModalDirective;
  @Output() change: EventEmitter<string> = new EventEmitter<string>();
  editPublisherForm: FormGroup;
  currentPublisher = new Publisher();


  constructor(public fb: FormBuilder, private api: ApiService) { }

  initialize(id: number): void {
    this.modal.show();
    this.api.getPublisher(id)
      .subscribe((data: Publisher) => {
        this.currentPublisher = data;
        this.currentPublisher.id = id;
        this.initializeFrom(this.currentPublisher);
      },
        (error: Error) => {
          console.log('err', error);

        });
  }

  initializeFrom(currentPublisher: Publisher) {
    this.editPublisherForm = this.fb.group({
      name: [currentPublisher.name, Validators.required],
      img: [currentPublisher.img],
    });
  }

  editPublisher() {
    const editedPublisher = new Publisher({
      id: this.currentPublisher.id,
      name: this.editPublisherForm.value.name,
      img: this.editPublisherForm.value.img
    });

    this.api.editPublisher(editedPublisher)
      .subscribe(() => {
        this.change.emit('publisher');
        this.modal.hide();
      },
        (error: Error) => {
          console.log('err', error);
        });
  }

  transformInNumberArray(string: string) {
    return JSON.parse('[' + string + ']');
  }

}
