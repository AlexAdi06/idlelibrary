import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { Publisher } from '../shared/publisher.model';
import { Writer } from '../shared/writer.model';
import { Book } from '../shared/book.model';
import { EditPublisherModalComponent } from './edit-publisher-modal/edit-publisher-modal.component';
import { EditWriterModalComponent } from './edit-writer-modal/edit-writer-modal.component';
import { EditBookModalComponent } from './edit-book-modal/edit-book-modal.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  publishers: Publisher[] = [];
  writers: Writer[] = [];
  books: Book[] = [];



  @ViewChild('editPublisherModal') editPublisherModal: EditPublisherModalComponent;
  @ViewChild('editWriterModal') editWriterModal: EditWriterModalComponent;
  @ViewChild('editBookModal') editBookModal: EditBookModalComponent;


  constructor(private api: ApiService) { }

  ngOnInit() {
    this.getPublishers();//getAlbums
    this.getWriters();//getArtists
    this.getBooks();//getSongs
  }

  getPublishers() {
    this.api.getPublishers()
      .subscribe((data: Publisher[]) => {
        this.publishers = [];

        for (let i = 0; i < data.length; i++) {
          this.api.getPublisher(data[i].id)
            .subscribe((info: Publisher) => {
              info.id = data[i].id;
              this.publishers.push(info);
            },
              (e: Error) => {
                console.log('err', e);
              });
        }

      },
        (error: Error) => {
          console.log('err', error);

        });
  }

  getWriters() {

    this.api.getWriters()
      .subscribe((data: Writer[]) => {
        this.writers = data;
      },
        (error: Error) => {
          console.log('err', error);
        });
  }

  getBooks() {
    this.api.getBooks()
      .subscribe((data: Book[]) => {
        this.books = data;
      },
        (error: Error) => {
          console.log('err', error);

        });
  }

  deletePublisher(id: number) {
    this.api.deletePublisher(id)
      .subscribe(() => {
        this.getPublishers();
      },
        (error: Error) => {
          console.log(error);
        });
  }

  deleteWriter(id: number) {
    this.api.deleteWriter(id)
      .subscribe(() => {
        this.getWriters();
      },
        (error: Error) => {
          console.log(error);
        });
  }

  deleteBook(id: number) {
    this.api.deleteBook(id)
      .subscribe(() => {
        this.getBooks();
      },
        (error: Error) => {
          console.log(error);
        });

  }

  showM1(id: number) {
    this.editPublisherModal.initialize(id);
  }

  showM2(id: number) {
    this.editWriterModal.initialize(id);
  }

  showM3(id: number) {
    this.editBookModal.initialize(id);
  }

  onEditFinished(event: string) {
    if (event === 'publisher') {
      this.getPublishers();
    }
    if (event === 'writer') {
      this.getWriters();
    }
    if (event === 'book') {
      this.getBooks();
    }


  }

}
