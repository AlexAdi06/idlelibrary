import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Book } from '../../shared/book.model';
import { ApiService } from '../../shared/api.service';

@Component({
  selector: 'app-edit-book-modal',
  templateUrl: './edit-book-modal.component.html',
  styleUrls: ['./edit-book-modal.component.css']
})
export class EditBookModalComponent {
  @ViewChild('editBookModal') modal: ModalDirective;
  @Output() change: EventEmitter<string> = new EventEmitter<string>();
  editBookForm: FormGroup;
  currentBook = new Book();

  constructor(public fb: FormBuilder, private api: ApiService) {}

  initialize(id: number): void {

    this.modal.show();
    this.api.getBook(id)
      .subscribe((data: Book) => {
        this.currentBook = data;
        this.initializeFrom(this.currentBook);
      },
        (error: Error) => {
          console.log('err', error);
        });
  }

  initializeFrom(currentBook: Book) {
    this.editBookForm = this.fb.group({
      name: [currentBook.name, Validators.required],
      popularity: [currentBook.popularity, Validators.required],
      publicationyear: [currentBook.publicationYear, Validators.required],
    });
  }

  editBook() {
    const editedBook = new Book({
      id: this.currentBook.id,
      name: this.editBookForm.value.name,
      popularity: this.editBookForm.value.popularity,
      publicationyear: this.editBookForm.value.publicationyear
    });

    this.api.editBook(editedBook)
      .subscribe(() => {
        this.change.emit('book');
        this.modal.hide();
      },
        (error: Error) => {
          console.log('err', error);
        });

  }

}
