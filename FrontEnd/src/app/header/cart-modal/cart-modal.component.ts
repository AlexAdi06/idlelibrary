import { Component, OnInit, ViewChild } from '@angular/core';
import { Publisher } from 'src/app/shared/publisher.model';
import { CartService } from 'src/app/shared/cart.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Book } from 'src/app/shared/book.model';

@Component({
  selector: 'app-cart-modal',
  templateUrl: './cart-modal.component.html',
  styleUrls: ['./cart-modal.component.css']
})
export class CartModalComponent implements OnInit {
  @ViewChild('cartModal') modal: ModalDirective;
  products: Book[] = [];


  constructor(private cartService: CartService) { }

  ngOnInit(): void {
  }

  initialize() {
    this.modal.show();
    this.products = this.cartService.get();
  }

  delete(id: number) {
    this.products.splice(id, 1);
  }
}
