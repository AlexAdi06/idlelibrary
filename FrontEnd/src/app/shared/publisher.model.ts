export class Publisher {
  id: number;
  name: string;
  img: string;
  bookPublisher: string[];

  constructor(input?: any) {
    Object.assign(this, input);
  }
}
