
import {Injectable} from '@angular/core';
import {Publisher} from './publisher.model';
import { Book } from './book.model';
import { Writer } from './writer.model';


@Injectable({
  providedIn: 'root'
})
export class CartService {
  publishers: Publisher[] =[];
  books: Book[] = [];
  writers: Writer[] = [];


  constructor() {

  }

  add(book: Book){
    this.books.push(book);

  }


  get() {
    return this.books;
  }


}
