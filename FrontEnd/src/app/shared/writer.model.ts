export class Writer {
  id: number;
  name: string;
  nationality: string;
  bookWriter:string[];
  writerCategory:string[];
  

  constructor(input?: any) {
    Object.assign(this, input);
  }
}
