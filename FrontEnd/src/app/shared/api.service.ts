import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Book } from './book.model';
import { Writer } from './writer.model';
import { Publisher } from './publisher.model';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {}

  header = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  baseUrl = 'https://localhost:44344/api';
 
  getPublisher(id: number) {
    return this.http.get(this.baseUrl + '/publisher/' + id.toString(), { headers: this.header });
  }

  getBook(id: number) {
    return this.http.get(this.baseUrl + '/book/' + id.toString(), { headers: this.header });
  }

  getWriter(id: number) {
    return this.http.get(this.baseUrl + '/writer/' + id.toString(), { headers: this.header });
  }

  //getStudio(id: number) {
   // return this.http.get(this.baseUrl + '/studio/' + id.toString(), { headers: this.header });
  //}



  getPublishers() {
    return this.http.get(this.baseUrl + '/publisher', { headers: this.header });
  }

  getBooks() {
    return this.http.get(this.baseUrl + '/book', { headers: this.header });
  }

  getWriters() {
    return this.http.get(this.baseUrl + '/writer', { headers: this.header });
  }



  addPublisher(publisher: Publisher) {
    return this.http.post(this.baseUrl + '/publisher', publisher, { headers: this.header });
  }

  addWriter(writer: Writer) {
    return this.http.post(this.baseUrl + '/writer', writer, { headers: this.header });
  }

 

  addBook(book) {
    return this.http.post(this.baseUrl + '/book', {
      'name': book.name,
      'popularity': book.popularity,
      'publicationYear': book.publicationYear,
      'writerId': JSON.parse('[' + book.writerId + ']'), 
      'libraryId': JSON.parse('[' + book.libraryId + ']'),
      'publisherId': JSON.parse('[' + book.publisherId + ']'),
      'img': book.img
    }, { headers: this.header });

  }



  deletePublisher(id: number) {
    return this.http.delete(this.baseUrl + '/publisher/' + id.toString(), { headers: this.header });
  }

  deleteBook(id: number) {
    return this.http.delete(this.baseUrl + '/book/' + id.toString(), { headers: this.header });
  }

  deleteWriter(id: number) {
    return this.http.delete(this.baseUrl + '/writer/' + id.toString(), { headers: this.header });
  }




  
  editPublisher(publisher: Publisher) {

    return this.http.put(this.baseUrl + '/publisher/' + publisher.id.toString(), publisher, { headers: this.header });
  }

  editWriter(writer: Writer) {
    return this.http.put(this.baseUrl + '/writer/' + writer.id.toString(), writer, { headers: this.header });
  }

  editBook(book: Book) {
    return this.http.put(this.baseUrl + '/book/' + book.id.toString(), book, { headers: this.header });
  }

}

