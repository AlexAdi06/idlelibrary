export class Book {
   id: number;
   name: string;
   popularity: string;
   publicationYear: number;
   writerName: string[];
   libraryName: string[];
   publisherName: string[];
   writerId:number[];
   libraryId:number[];
   publisherId:number[];
   img: string;

  constructor(input?: any) {
    Object.assign(this, input);
  }
}
